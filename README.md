[![build status](https://gitlab.com/SadatAnwar/transaction-stats/badges/master/build.svg)](https://gitlab.com/SadatAnwar/transaction-stats/commits/master)

# Specs

### POST `/transactions`

#### Body:

```json 
{
"amount": 12.3, 
"timestamp": 1478192204000
}
```
#### Result
0. 201 - in case of success
0. 204 - if transaction is older than 60 seconds
0. **NOTE** (400 - if request is invalid?) This is not specified, but does make sense (eg: null timestamp or null amount)


### GET `/statistics`
Should operate in **O(1)** time and space complexity. And return the below statistics for the transactions in the past 60 seconds. 

**NOTE:** Since there is no mention on how to handle timestamps from the future, I chose to save them, but do not contribute to the statistics
see this [test](/src/test/java/com/n26/transactionstatistics/store/TransactionStatisticsStoreComponentTest.java#L121)


Returns: 
```json
{
"sum": 1000.0, 
"avg": 100.0, 
"max": 200.0, 
"min": 50.0, 
"count": 10
}
```

# Implementation 

## Description
The implementation is done using Spring-Boot (1.5 downloaded from [spring.io](https://start.spring.io/).
As required once the application is running, it exposes 2 REST endpoints
 0. `POST` `/transactions` that is used to add a new transaction 
 0. `GET` `/statistics` that is used to get the statistics of the last 60 seconds
 
## Try-out
0. Clone the repo by `git clone git@gitlab.com:SadatAnwar/transaction-stats.git`
0. Once you have cloned the repo enter the directory `cd transaction-stats`
0. Clean and run tests `mvn clean test` (You should have a Build Success at the end)
0. Start the server `mvn spring-boot:run`
0. The application should be available at `localhost:8080` (if you wish to change the port modify the server.port config in the [config file](/src/main/resources/application.properties)

## Implementation Details
The over-all requirements are quite straight forward, and do not need much explanations.
I have tried to follow the clean-code (By Robert C. Martin), and have aimed for very self explanatory classes and methods.

All classes and methods are thoroughly tested (100% code coverage). These tests can be found [here](/src/test/java/com/n26/transactionstatistics).

### O(1) Time and and Space complexity requirement
The most challenging part of the exercise is the constant time and memory limitation.
To achieve this I have implemented the [`TransactionStatisticsStore.java`](/src/main/java/com/n26/transactionstatistics/store/TransactionStatisticsStore.java)
that uses a `ConcurrentNavigableMap` (implemented as `ConcurrentSkipListMap`). 
The map uses the time stamp in milliseconds as the `key` and a StatisticalSummary (`DoubleSummaryStatistics`) as the value. 

Though the use of the Map is not O(1) in it-self, our requirement requires computing the statistics of only a fixed time interval (60 seconds),
this sets the upper limit to the maximum number of elements that can be in this map at any given time to 60,000.
The time and space complexity of the implementation is therefore not related to the number of requests served in the 60 seconds.
