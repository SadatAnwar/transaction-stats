package com.n26.transactionstatistics.store;

import com.n26.transactionstatistics.model.Transaction;
import com.n26.transactionstatistics.model.TransactionStats;
import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

import java.util.DoubleSummaryStatistics;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

@Component
public class TransactionStatisticsStore
{
    public static final int HISTORY_DURATION = 60000;

    private ConcurrentNavigableMap<Long, DoubleSummaryStatistics> transactionBuckets = new ConcurrentSkipListMap<>();

    /**
     * Insert a new transaction into the store. This method ensures the transaction is not older than 60 seconds
     * before inserting it into the store. Once a new transaction is inserted, it only updates the Statistical summary
     * of the transaction timeStamp. The update is synchronized to ensure thread safety.
     * <p>
     * After every update the store clears any data that is already too told (older than 60 seconds)
     */
    public void insert(Transaction transaction)
    {
        long now = DateTime.now().getMillis();
        long transactionTimestamp = transaction.getTimestamp();

        // Make sure the data is not stale
        long ageOfTransaction = now - transactionTimestamp;
        if (ageOfTransaction > HISTORY_DURATION) {
            return;
        }

        //make sure the a DoubleSummaryStatistics object is present (executed atomically)
        transactionBuckets.putIfAbsent(transactionTimestamp, new DoubleSummaryStatistics());

        updateStatistics(transaction);

        // Clean-up any data that might be stale already
        long ageOfOldestEntry = now - transactionBuckets.firstKey();
        if (ageOfOldestEntry > HISTORY_DURATION) {
            cleanUp(now - HISTORY_DURATION);
        }
    }

    /**
     * Returns a TransactionStats object that represents the statistics for the last 60 seconds
     * Since the maximum number of elements in the transactionBuckets is limited to 60,000 the time and space complexity
     * of the operation is not dependant to the number of transactions
     *
     * @return TransactionStats for the last 60 seconds
     */
    public TransactionStats getStatistics()
    {
        long now = DateTime.now().getMillis();

        // Check if store is empty OR the newest entry is too old
        if (transactionBuckets.isEmpty() || now - transactionBuckets.lastKey() > HISTORY_DURATION) {
            return new TransactionStats(new DoubleSummaryStatistics());
        }

        DoubleSummaryStatistics stats = new DoubleSummaryStatistics();
        transactionBuckets.tailMap(now - HISTORY_DURATION).headMap(now, true).values()
            .forEach(stats::combine);

        return new TransactionStats(stats);
    }

    /**
     * Updates the current statistics with the new incoming transaction.
     * <p>
     * Synchronization is needed on the statistics object as its not thread safe.
     */
    private void updateStatistics(Transaction transaction)
    {
        DoubleSummaryStatistics statistics = transactionBuckets.get(transaction.getTimestamp());

        synchronized (statistics) {
            statistics.accept(transaction.getAmount());
        }
    }

    /**
     * Remove all data in the store that is older than given time stamp
     */
    private void cleanUp(long olderThan)
    {
        transactionBuckets.headMap(olderThan).keySet().forEach(transactionBuckets::remove);
    }
}
