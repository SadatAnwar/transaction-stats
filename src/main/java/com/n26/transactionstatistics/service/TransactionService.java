package com.n26.transactionstatistics.service;

import com.n26.transactionstatistics.exception.StaleTransactionException;
import com.n26.transactionstatistics.model.Transaction;
import com.n26.transactionstatistics.model.TransactionStats;
import com.n26.transactionstatistics.store.TransactionStatisticsStore;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionService
{
    private final TransactionStatisticsStore transactionStatisticsStore;

    @Autowired
    public TransactionService(TransactionStatisticsStore transactionStatisticsStore)
    {
        this.transactionStatisticsStore = transactionStatisticsStore;
    }

    public void insertTransaction(Transaction transaction)
    {
        if (isStale(transaction)) {
            throw new StaleTransactionException();
        }

        transactionStatisticsStore.insert(transaction);
    }

    public TransactionStats getStatistics()
    {
        return transactionStatisticsStore.getStatistics();
    }

    private boolean isStale(Transaction transaction)
    {
        return DateTime.now().getMillis() - transaction.getTimestamp() > TransactionStatisticsStore.HISTORY_DURATION;
    }
}
