package com.n26.transactionstatistics.model;

import java.util.DoubleSummaryStatistics;

public class TransactionStats
{
    private double sum;

    private double avg;

    private long count;

    private double min;

    private double max;

    public TransactionStats(DoubleSummaryStatistics stats)
    {
        this.sum = stats.getSum();
        this.avg = stats.getAverage();
        this.count = stats.getCount();
        this.min = stats.getMin() == Double.POSITIVE_INFINITY ? 0D : stats.getMin();
        this.max = stats.getMax() == Double.NEGATIVE_INFINITY ? 0D : stats.getMax();
    }

    public double getSum()
    {
        return sum;
    }

    public double getAvg()
    {
        return avg;
    }

    public double getMax()
    {
        return max;
    }

    public double getMin()
    {
        return min;
    }

    public long getCount()
    {
        return count;
    }
}
