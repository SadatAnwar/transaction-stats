package com.n26.transactionstatistics.controller;

import com.n26.transactionstatistics.model.TransactionStats;
import com.n26.transactionstatistics.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequestMapping(value = "/statistics")
public class StatisticsController
{
    private final TransactionService transactionService;

    @Autowired
    public StatisticsController(TransactionService transactionService)
    {
        this.transactionService = transactionService;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET)
    public TransactionStats getStatistics()
    {
        return transactionService.getStatistics();
    }
}
