package com.n26.transactionstatistics.integrationtests.controller;

import com.n26.transactionstatistics.integrationtests.BaseIntegrationTest;
import com.n26.transactionstatistics.model.Transaction;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.Test;
import org.springframework.test.annotation.DirtiesContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class StatisticsControllerIntegrationTest extends BaseIntegrationTest
{
    private static final String CONTROLLER_ENDPOINT = "/statistics";

    private static final String TRANSACTION_ENDPOINT = "/transactions";

    @Test
    @DirtiesContext
    public void GET_statistics_should_return_0_stats_if_no_transactions_saved() throws Exception
    {
        mockMvc.perform(get(CONTROLLER_ENDPOINT))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.count").value(0))
            .andExpect(jsonPath("$.sum").value(0))
            .andExpect(jsonPath("$.avg").value(0))
            .andExpect(jsonPath("$.min").value(0))
            .andExpect(jsonPath("$.max").value(0));
    }

    @Test
    @DirtiesContext
    public void GET_should_compute_stats_for_transactions_saved() throws Exception
    {
        mockMvc.perform(postWithDto(TRANSACTION_ENDPOINT, new Transaction(100D, DateTime.now().getMillis())));
        mockMvc.perform(postWithDto(TRANSACTION_ENDPOINT, new Transaction(99D, DateTime.now().getMillis())));
        mockMvc.perform(postWithDto(TRANSACTION_ENDPOINT, new Transaction(101D, DateTime.now().getMillis())));

        mockMvc.perform(get(CONTROLLER_ENDPOINT))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.count").value(3))
            .andExpect(jsonPath("$.sum").value(300))
            .andExpect(jsonPath("$.avg").value(100))
            .andExpect(jsonPath("$.min").value(99))
            .andExpect(jsonPath("$.max").value(101));
    }

    @Test
    @DirtiesContext
    public void GET_should_not_compute_stats_for_stale_transactions_saved() throws Exception
    {
        long start = DateTime.now().getMillis();
        mockMvc.perform(postWithDto(TRANSACTION_ENDPOINT, new Transaction(100D, start)));
        mockMvc.perform(postWithDto(TRANSACTION_ENDPOINT, new Transaction(99D, start + 1)));
        mockMvc.perform(postWithDto(TRANSACTION_ENDPOINT, new Transaction(101D, start + 2)));

        //Fast-forward the clock
        DateTimeUtils.setCurrentMillisFixed(start + 60001);

        mockMvc.perform(get(CONTROLLER_ENDPOINT))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.count").value(2))
            .andExpect(jsonPath("$.sum").value(200))
            .andExpect(jsonPath("$.avg").value(100))
            .andExpect(jsonPath("$.min").value(99))
            .andExpect(jsonPath("$.max").value(101));
    }

    @Test
    @DirtiesContext
    public void GET_should_not_compute_stats_for_future_transactions_saved() throws Exception
    {
        long start = DateTime.now().getMillis();
        mockMvc.perform(postWithDto(TRANSACTION_ENDPOINT, new Transaction(100D, start + 10000)));
        mockMvc.perform(postWithDto(TRANSACTION_ENDPOINT, new Transaction(99D, start)));
        mockMvc.perform(postWithDto(TRANSACTION_ENDPOINT, new Transaction(101D, start)));

        mockMvc.perform(get(CONTROLLER_ENDPOINT))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.count").value(2))
            .andExpect(jsonPath("$.sum").value(200))
            .andExpect(jsonPath("$.avg").value(100))
            .andExpect(jsonPath("$.min").value(99))
            .andExpect(jsonPath("$.max").value(101));
    }
}
