package com.n26.transactionstatistics.integrationtests.controller;

import com.n26.transactionstatistics.integrationtests.BaseIntegrationTest;
import com.n26.transactionstatistics.model.Transaction;
import org.joda.time.DateTime;
import org.junit.Test;
import org.springframework.test.annotation.DirtiesContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class TransactionControllerIntegrationTest extends BaseIntegrationTest
{
    private static final String CONTROLLER_ENDPOINT = "/transactions";

    @Test
    public void POST_with_valid_data_should_should_return_201() throws Exception
    {
        long now = DateTime.now().getMillis();
        Transaction transaction = new Transaction(100.0, now);

        mockMvc.perform(postWithDto(CONTROLLER_ENDPOINT, transaction))
            .andExpect(status().is(201));
    }

    @Test
    public void POST_with_stale_data_should_return_status_204() throws Exception
    {
        long now = DateTime.now().getMillis();
        Transaction transaction = new Transaction(100.0, now - 60001);

        mockMvc.perform(postWithDto(CONTROLLER_ENDPOINT, transaction))
            .andExpect(status().is(204));
    }

    @Test
    public void POST_with_invalid_data_should_return_status_bad_request() throws Exception
    {
        Transaction transaction = new Transaction();

        mockMvc.perform(postWithDto(CONTROLLER_ENDPOINT, transaction))
            .andExpect(status().isBadRequest());
    }
}
