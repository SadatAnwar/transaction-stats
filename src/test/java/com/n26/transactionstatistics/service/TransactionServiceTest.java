package com.n26.transactionstatistics.service;

import com.n26.transactionstatistics.exception.StaleTransactionException;
import com.n26.transactionstatistics.model.Transaction;
import com.n26.transactionstatistics.model.TransactionStats;
import com.n26.transactionstatistics.store.TransactionStatisticsStore;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.DoubleSummaryStatistics;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTest
{
    @InjectMocks
    private TransactionService subject;

    @Mock
    private TransactionStatisticsStore transactionStatisticsStore;

    @Test(expected = StaleTransactionException.class)
    public void insertTransaction_should_throw_exception_if_transaction_is_too_old() throws Exception
    {
        long now = DateTime.now().getMillis();
        Transaction transaction = new Transaction(100D, now - 60001);
        subject.insertTransaction(transaction);
    }

    @Test
    public void isStale_should_insert_new_transaction_into_store() throws Exception
    {
        long now = DateTime.now().getMillis();
        Transaction transaction = new Transaction(100D, now);
        subject.insertTransaction(transaction);

        verify(transactionStatisticsStore).insert(eq(transaction));
    }

    @Test
    public void getStats_should_get_stats_from_store() throws Exception
    {
        DoubleSummaryStatistics stats = new DoubleSummaryStatistics();
        stats.accept(100);

        when(transactionStatisticsStore.getStatistics()).thenReturn(new TransactionStats(stats));

        TransactionStats statistics = subject.getStatistics();

        assertEquals(1, statistics.getCount());
        assertEquals(100, statistics.getSum(), 0);
    }
}
