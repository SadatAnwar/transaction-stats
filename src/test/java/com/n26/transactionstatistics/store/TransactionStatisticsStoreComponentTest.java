package com.n26.transactionstatistics.store;

import com.n26.transactionstatistics.model.Transaction;
import com.n26.transactionstatistics.model.TransactionStats;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.DoubleSummaryStatistics;
import java.util.concurrent.ConcurrentNavigableMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TransactionStatisticsStoreComponentTest
{
    private TransactionStatisticsStore subject;

    @Before
    public void setUp() throws Exception
    {
        subject = new TransactionStatisticsStore();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void insert_should_add_element_to_navigable_map()
    {
        long now = DateTime.now().getMillis();
        subject.insert(new Transaction(100D, now));
        subject.insert(new Transaction(100D, now - 10000));
        subject.insert(new Transaction(100D, now - 50000));
        subject.insert(new Transaction(100D, now + 30000));

        Object transactionBuckets = ReflectionTestUtils.getField(subject, "transactionBuckets");
        ConcurrentNavigableMap<Long, DoubleSummaryStatistics> map = (ConcurrentNavigableMap<Long, DoubleSummaryStatistics>) transactionBuckets;

        assertTrue(map.containsKey(now));
        assertTrue(map.containsKey(now - 10000));
        assertTrue(map.containsKey(now - 50000));
        assertTrue(map.containsKey(now + 30000));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void insert_should_remove_stale_elements_from_sorted_map()
    {
        Object transactionBuckets = ReflectionTestUtils.getField(subject, "transactionBuckets");
        ConcurrentNavigableMap<Long, DoubleSummaryStatistics> map = (ConcurrentNavigableMap<Long, DoubleSummaryStatistics>) transactionBuckets;

        long timeStamp = DateTime.now().getMillis();
        subject.insert(new Transaction(100D, timeStamp));

        assertTrue(map.containsKey(timeStamp));

        //Fast-forward 60.01 seconds
        long future = timeStamp + 60001;
        DateTimeUtils.setCurrentMillisFixed(future);

        subject.insert(new Transaction(100D, future));

        assertFalse(map.containsKey(timeStamp));
    }

    @Test
    public void insert_multiple_transaction_with_same_valid_time_stamps_should_update_stats() throws Exception
    {
        long now = DateTime.now().getMillis();
        subject.insert(new Transaction(99D, now));
        subject.insert(new Transaction(100D, now));
        subject.insert(new Transaction(101D, now));

        TransactionStats result = subject.getStatistics();

        assertEquals(300D, result.getSum(), 0.0);
        assertEquals(3, result.getCount());
        assertEquals(100, result.getAvg(), 0.0);
    }

    @Test
    public void get_stats_should_not_insert_element_that_is_already_stale() throws Exception
    {
        long now = DateTime.now().getMillis();

        subject.insert(new Transaction(101D, now - 60001));

        TransactionStats result = subject.getStatistics();

        assertEquals(0, result.getCount());
    }

    @Test
    public void get_stats_should_not_return_stats_for_elements_that_have_expired() throws Exception
    {
        long now = DateTime.now().getMillis();

        //Freeze time
        DateTimeUtils.setCurrentMillisFixed(now + 2);

        subject.insert(new Transaction(99D, now));
        subject.insert(new Transaction(100D, now + 1));
        subject.insert(new Transaction(101D, now + 2));

        TransactionStats result = subject.getStatistics();

        assertEquals(3, result.getCount());

        //Fast forward 60.01 seconds
        DateTimeUtils.setCurrentMillisFixed(now + 60001);

        result = subject.getStatistics();

        assertEquals(2, result.getCount());
        assertEquals(201, result.getSum(), 0.0);
    }

    @Test
    public void get_stats_should_not_return_stats_for_future() throws Exception
    {
        long now = DateTime.now().getMillis();

        //Freeze time
        DateTimeUtils.setCurrentMillisFixed(now);

        subject.insert(new Transaction(99D, now));
        subject.insert(new Transaction(100D, now + 1000));
        subject.insert(new Transaction(101D, now + 2000));

        TransactionStats result = subject.getStatistics();

        assertEquals(1, result.getCount());
        assertEquals(99, result.getSum(), 0);

        //Fast-forward 2000 ms
        DateTimeUtils.setCurrentMillisFixed(now + 2000);

        result = subject.getStatistics();

        assertEquals(3, result.getCount());
        assertEquals(300, result.getSum(), 0);
    }
}
